Source: libkavorka-perl
Section: perl
Priority: optional
Build-Depends:
 debhelper-compat (= 12),
 libclass-method-modifiers-perl <!nocheck>,
 libclass-tiny-perl <!nocheck>,
 libdatetime-perl <!nocheck>,
 libexporter-tiny-perl,
 libmatch-simple-perl,
 libmodule-runtime-perl,
 libmoo-perl,
 libmoops-perl <!nocheck>,
 libmoose-perl <!nocheck>,
 libmoosex-types-perl <!nocheck>,
 libmouse-perl <!nocheck>,
 libnamespace-sweep-perl,
 libpadwalker-perl,
 libparse-keyword-perl,
 libreturn-type-perl,
 librole-tiny-perl <!nocheck>,
 libtest-fatal-perl <!nocheck>,
 libtest-requires-perl <!nocheck>,
 libtype-tiny-perl,
 perl | libdata-alias-perl,
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libkavorka-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libkavorka-perl
Homepage: https://metacpan.org/release/Kavorka
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-perl

Package: libkavorka-perl
Architecture: all
Depends:
 libexporter-tiny-perl,
 libmatch-simple-perl,
 libmodule-runtime-perl,
 libmoo-perl,
 libnamespace-sweep-perl,
 libpadwalker-perl,
 libparse-keyword-perl,
 libreturn-type-perl,
 libtype-tiny-perl,
 perl | libdata-alias-perl,
 ${misc:Depends},
 ${perl:Depends},
Recommends:
 libtype-tiny-xs-perl,
Suggests:
 libmoose-perl,
Description: function signatures with the lure of the animal
 Kavorka provides "fun" and "method" keywords for declaring functions
 and methods.  It uses Perl 5.14's keyword API, so should work more
 reliably than source filters or Devel::Declare-based modules.
 .
 The syntax provided by Kavorka is largely inspired by Perl 6, though it
 has also been greatly influenced by Method::Signatures and
 Function::Parameters.
